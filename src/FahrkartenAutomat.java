import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       // Achtung!!!!
       // Hier wird deutlich, warum double nicht f�r Geldbetr�ge geeignet ist.
       // =======================================
       double r�ckgabebetrag;
       double zuZahlenderBetrag;
       
       while(true) {
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(r�ckgabebetrag);
       ausschalten();
       }
}
    
    public static double fahrkartenbestellungErfassen() {
        // Den zu zahlenden Betrag ermittelt normalerweise der Automat
        // aufgrund der gew�hlten Fahrkarte(n).
        // -----------------------------------
    	Scanner tastatur = new Scanner(System.in);
        System.out.print("Zu zahlender Betrag (EURO): ");
        double ticketPreis = tastatur.nextDouble();
        System.out.print("Anzahl der Tickets: ");
        int anzahlTickets = tastatur.nextInt();
        double zuZahlenderBetrag = ticketPreis * anzahlTickets;
        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
        // Geldeinwurf
        // -----------
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   double eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben() {
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    public static void rueckgeldAusgeben (double r�ckgabebetrag) {
        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO\n", r�ckgabebetrag);
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
        }
       
        
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.");
    }
    public static boolean ausschalten(){
        Scanner tastatur = new Scanner(System.in);
        System.out.print("Noch eine fahrkarte (J/N): ");
        char antwort = tastatur.next().charAt(0);
        if(antwort=='j') {
            return false;
        } else {
            return true;
    	
    	
    	
    	
    }
}

}
